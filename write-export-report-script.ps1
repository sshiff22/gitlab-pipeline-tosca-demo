$tscFile='export-report.tsc'

$timeStamp=Get-Date -Format "MMM dd, yyyy @ hh:mm:ss"

#if (Test-Path -Path $tscFile) {rm $tscFile}

Add-Content $tscFile "// $tscFile //"
Add-Content $tscFile "// This file was created on $timeStamp "
Add-Content $tscFile "//#########################################################//"
Add-Content $tscFile "`n"
Add-Content $tscFile "// Update Workscae from repository //"
Add-Content $tscFile UpdateAll
Add-Content $tscFile "`n"

Add-Content $tscFile "// Navigate to the Execution List to run and one node up (to point to the folder) //"
Add-Content $tscFile "jumpToNode `"/Execution/DEX_CI_List/CI_ExecutionList`""
#Add-Content $tscFile "JumpTo =>Items:ExecutionList[UniqueId==`"39ebfb4b-58a2-5760-1e74-23aa74fcb681`"]"

#Add-Content $tscFile "JumpTo =>SUBPARTS:ExecutionList[(Name=?`"Regression_Tests`")]"
Add-Content $tscFile "`n"
Add-Content $tscFile "// Run print report task //"
Add-Content $tscFile "task `"Print Report ... ExecutionEntries with detailed Logs Modified`""

if ($env:CI_PROJECT_DIR) {$exportLocation=$env:CI_PROJECT_DIR}
else  {$exportLocation = split-path -parent $MyInvocation.MyCommand.Definition}

$timeStamp=Get-Date -Format "yyyyMMddTHHmmssffff"
$exportFileName="RunReport_$timeStamp.pdf"
$exportLocation=$exportLocation+"\Results\"
$exportLocation=$exportLocation.Replace("\","\\")

#Write-Host "$($exportLocation)$($exportFileName)"
#Write-Host "exportFileName: $exportFileName"
#Write-Host "timeStamp: $timeStamp"

Add-Content $tscFile "$($exportLocation)$($exportFileName)"
Add-Content $tscFile "`n"
Add-Content $tscFile "// Exit TCShell //"
Add-Content $tscFile exit
Add-Content $tscFile "`n"
Add-Content $tscFile "// Confirm exit //"
Add-Content $tscFile y
